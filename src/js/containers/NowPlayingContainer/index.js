import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { getNowPlaying } from '../../actions';
import MovieListComponent from '../../components/MovieListComponent';

class NowPlayingContainer extends Component {
  static propTypes = {
    movies: PropTypes.object,
    getNowPlaying: PropTypes.func,
  };

  componentDidMount() {
    this.props.getNowPlaying();
  }

  render() {
    const { data: { results }, loaded, loading } = this.props.playing;

    return (
      <MovieListComponent
        results={results}
        loaded={loaded}
        loading={loading}
      />
    );
  }
}

const mapStateToProps = (state) => {
  return {
    playing: state.get.nowPlaying,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getNowPlaying: () => dispatch(getNowPlaying()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NowPlayingContainer);
