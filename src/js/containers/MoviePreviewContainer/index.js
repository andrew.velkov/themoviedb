import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { Button, Grid, Typography } from '@material-ui/core';

import { getMovie, getSimilarMovies } from '../../actions';
import MovieListComponent from '../../components/MovieListComponent';
import Fetching from '../../components/Fetching';

class MoviePreviewContainer extends Component {
  static propTypes = {
    movies: PropTypes.object,
    similarMovies: PropTypes.object,
    getMovie: PropTypes.func,
    getSimilarMovies: PropTypes.func,
    movieId: PropTypes.string,
  };

  componentDidMount() {
    const { movieId, getMovie } = this.props;
    getMovie(movieId);
  }

  similarMoviesHandler = () => {
    const { movieId, getSimilarMovies } = this.props;
    getSimilarMovies(movieId);
  }

  render() {
    const { data, loading } = this.props.movie;
    const { data: { results }, loaded, loading: loadingSimilar } = this.props.similarMovies;
    const base_url = 'https://image.tmdb.org/t/p/w500/';
    console.log('props', this.props);

    return (
      <section>
        <Fetching isFetching={loading}>
          <Grid container alignItems="center" spacing={24}>
            <Grid item xs={3}>{data.poster_path && <img src={`${base_url}${data.poster_path}`} alt="" />}</Grid>
            <Grid item xs={9}>
              <h3>{data.title}</h3>
              <Typography>{data.overview}</Typography>
              <br/>
              <Grid container spacing={24}>
                <Grid item>
                  <NavLink onClick={this.clear} to="/i/movie">
                    <Button variant="contained">Back to home</Button>
                  </NavLink>
                </Grid>
                <Grid item>
                  <Button onClick={this.similarMoviesHandler}>
                    Similar movies
                  </Button>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Fetching>

        <MovieListComponent
          results={results}
          loaded={loaded}
          loading={loadingSimilar}
          overflow
        />

      </section>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    movie: state.get.movie,
    similarMovies: state.get.similar,
    movieId: props.match.params.id,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getMovie: (movieId) => dispatch(getMovie(movieId)),
    getSimilarMovies: (movieId) => dispatch(getSimilarMovies(movieId)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MoviePreviewContainer);
