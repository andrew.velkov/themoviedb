import React, { Component } from 'react';
import PropTypes from 'prop-types';
import debounce from 'debounce';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';

import { getMovies } from '../../actions';
import MovieInputSearchComponent from '../../components/MovieInputSearchComponent';
import MovieListComponent from '../../components/MovieListComponent';
import Pagination from '../../components/Pagination';

class MovieSearchContainer extends Component {
  static propTypes = {
    movies: PropTypes.object,
    getMovies: PropTypes.func,
  };

  state = {
    searchDefault: 'Bruce Lee',
    selectedPage: 1,
  }

  componentDidMount() {
    const { searchDefault } = this.state;
    this.props.getMovies(searchDefault);
  }

  componentWillUnmount() {
    this.debounceEvent = false;
  }

  debounceEvent = (...args) => {
    const debounceEvent = debounce(...args);

    return (event) => {
      event.persist();
      return debounceEvent(event);
    };
  }

  handleChangeSearch = (e) => {
    const { searchDefault } = this.state;
    const { value } = e.target;
    const search = value === '' ? searchDefault : value;

    this.setState({
      searchDefault: value,
    });
    this.props.getMovies(search);
  }

  setPagination = (selectedPage) => {
    const { searchDefault } = this.state;

    this.setState({
      selectedPage,
    });
    this.props.getMovies(searchDefault, selectedPage);
  };

  render() {
    const { data: { results, total_results }, loaded, loading } = this.props.movies;

    return (
      <React.Fragment>
        <NavLink to="/i/movie/now-playing">Movies now playing</NavLink>

        <MovieInputSearchComponent
          onChange={this.debounceEvent(this.handleChangeSearch, 500)}
        />

        <MovieListComponent
          results={results}
          loaded={loaded}
          loading={loading}
        />

        {(results && results.length > 0) && <Pagination
          pageCount={total_results / 20}
          handleSetPage={this.setPagination}
        />}

      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    movies: state.get.movies,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getMovies: (search, page) => dispatch(getMovies(search, page)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MovieSearchContainer);
