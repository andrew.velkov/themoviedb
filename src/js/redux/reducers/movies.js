const initialState = {
  loaded: false,
  loading: false,
  data: [],
};

export default function movie(movieName = '') {
  return (state = initialState, action = {}) => {
    console.log('ac', action);
    switch (action.type) {
      case `themoviedb/${movieName}/LOAD`:
        return {
          ...state,
          loading: true,
          loaded: false,
        };
      case `themoviedb/${movieName}/LOAD_SUCCESS`:
        return {
          ...state,
          loading: false,
          loaded: true,
          data: action.payload,
        };
      case `themoviedb/${movieName}/LOAD_ERROR`:
        return {
          ...state,
          loading: false,
          loaded: false,
        };
      default:
        return state;
    }
  };
};