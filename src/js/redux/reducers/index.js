import { combineReducers } from 'redux';

import movies from './movies';

const reducers = combineReducers({
  get: combineReducers({
    movies: movies('movies'),
    movie: movies('movie'),
    nowPlaying: movies('nowPlaying'),
    similar: movies('similar'),
  }),
});

export default reducers;
