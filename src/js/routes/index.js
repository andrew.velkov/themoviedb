import React, { Component } from 'react';
import { Router, Route, Switch } from 'react-router-dom';

import MovieSearchPage from '../pages/MovieSearchPage';
import MoviePreviewPage from '../pages/MoviePreviewPage';
import NowPlayingPage from '../pages/NowPlayingPage';
import NotFoundPage from '../pages/NotFoundPage';

import history from './history';

export default class Routes extends Component {
  render() {
    return (
      <Router history={ history }>
        <Switch>
          <Route exact path='/i/movie/' render={ (props) => <MovieSearchPage {...props} /> } />
          <Route path='/i/movie/id/:id' render={ (props) => <MoviePreviewPage key={props.location.pathname} {...props} /> } />
          <Route path='/i/movie/now-playing' render={ (props) => <NowPlayingPage {...props} /> } />
          <Route component={ NotFoundPage } />
        </Switch>
      </Router>
    );
  }
}
