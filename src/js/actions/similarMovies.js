import * as type from '../constants/movies';
import themoviedb from '../config/themoviedb';

export const getSimilarMovies = (movie_id) => {
  return {
    types: [type.GET_SIMILAR_MOVIES, type.GET_SIMILAR_MOVIES_SUCCESS, type.GET_SIMILAR_MOVIES_ERROR],
    request: {
      method: 'GET',
      url: `${themoviedb.base_url}/movie/${movie_id}/similar?api_key=${themoviedb.api_key}`,
    },
  };
};
