import { getMovies, getMovie } from './movies';
import { getNowPlaying } from './nowPlaying';
import { getSimilarMovies } from './similarMovies';

export {
  getMovies,
  getMovie,
  getNowPlaying,
  getSimilarMovies,
};
