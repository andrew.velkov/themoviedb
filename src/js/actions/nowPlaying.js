import * as type from '../constants/movies';
import themoviedb from '../config/themoviedb';

export const getNowPlaying = (lang = 'en-US', page = 1) => {
  return {
    types: [type.GET_NOW_PLAYING, type.GET_NOW_PLAYING_SUCCESS, type.GET_NOW_PLAYING_ERROR],
    request: {
      method: 'GET',
      url: `${themoviedb.base_url}/movie/now_playing?api_key=${themoviedb.api_key}&language=${lang}&page=${page}`,
    },
  };
};
