import * as type from '../constants/movies';
import themoviedb from '../config/themoviedb';

export const getMovies = (search = '', page = 1) => {
  return {
    types: [type.GET_MOVIES, type.GET_MOVIES_SUCCESS, type.GET_MOVIES_ERROR],
    request: {
      method: 'GET',
      url: `${themoviedb.base_url}/search/movie?api_key=${themoviedb.api_key}&query=${search}&page=${page}`,
    },
  };
};

export const getMovie = (movie_id) => {
  return {
    types: [type.GET_MOVIE, type.GET_MOVIE_SUCCESS, type.GET_MOVIE_ERROR],
    request: {
      method: 'GET',
      url: `${themoviedb.base_url}/movie/${movie_id}?api_key=${themoviedb.api_key}`,
    },
  };
};
