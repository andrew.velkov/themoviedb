export const GET_MOVIES = 'themoviedb/movies/LOAD';
export const GET_MOVIES_SUCCESS = 'themoviedb/movies/LOAD_SUCCESS';
export const GET_MOVIES_ERROR = 'themoviedb/movies/LOAD_ERROR';

export const GET_MOVIE = 'themoviedb/movie/LOAD';
export const GET_MOVIE_SUCCESS = 'themoviedb/movie/LOAD_SUCCESS';
export const GET_MOVIE_ERROR = 'themoviedb/movie/LOAD_ERROR';

export const GET_NOW_PLAYING = 'themoviedb/nowPlaying/LOAD';
export const GET_NOW_PLAYING_SUCCESS = 'themoviedb/nowPlaying/LOAD_SUCCESS';
export const GET_NOW_PLAYING_ERROR = 'themoviedb/nowPlaying/LOAD_ERROR';

export const GET_SIMILAR_MOVIES = 'themoviedb/similar/LOAD';
export const GET_SIMILAR_MOVIES_SUCCESS = 'themoviedb/similar/LOAD_SUCCESS';
export const GET_SIMILAR_MOVIES_ERROR = 'themoviedb/similar/LOAD_ERROR';
