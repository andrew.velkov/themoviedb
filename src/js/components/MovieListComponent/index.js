import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Grid, Button, Typography } from '@material-ui/core';
import LazyLoad from 'react-lazy-load';

import Fetching from '../../components/Fetching';

const base_url = 'https://image.tmdb.org/t/p/w185_and_h278_bestv2';

const MovieListComponent = ({ results, loaded, loading, overflow }) => (
  <section style={overflow && {overflowY: "auto", maxHeight: 500, padding: "0 16px"}}>
    <Fetching isFetching={loading}>
      {loaded && results.map(movie => {
        return (
          <LazyLoad key={movie.id} throttle={100} height={300}>
            <Grid container spacing={24}>
              <Grid item xs={12} sm={3} md={2}>
                {movie.poster_path
                  ? <img src={`${base_url}${movie.poster_path}`} alt="" />
                  : <div style={{backgroundColor: "#eee", width: "100%", maxWidth: 185, height: "100%", minHeight: 100}} />
                }
              </Grid>

              <Grid item xs={12} sm={9} md={10}>
                <h3>{movie.title}</h3>
                <Typography>{movie.overview}</Typography>
                <br/>
                <Grid container item spacing={24} xs={12}>
                  <Grid item>
                    <Link to={`/i/movie/id/${movie.id}`}>
                      <Button variant="contained" color="primary">Preview</Button>
                    </Link>
                  </Grid>
                  <Grid item>
                    <Button target="_blank" variant="contained" href={`https://www.themoviedb.org/movie/${movie.id}`}>
                      Preview 2
                    </Button>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </LazyLoad>
        );
      })}
    </Fetching>

    {(!loaded || results.length === 0) && <p style={{color: "#DEDEDE", textAlign: "center"}}>
      No results...
    </p>}
  </section>
);

MovieListComponent.propTypes = {
  results: PropTypes.array,
  loaded: PropTypes.bool,
  loading: PropTypes.bool,
  overflow: PropTypes.bool,
};

export default MovieListComponent;