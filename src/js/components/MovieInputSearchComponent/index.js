import React from 'react';
import PropTypes from 'prop-types';
import { TextField } from '@material-ui/core';

const MovieInputSearchComponent = ({ onChange }) => (
  <TextField
    label="Search movies..."
    placeholder='e.g: 劇場版 七つの大罪 天空の囚われ人.'
    name="search"
    type="search"
    margin="normal"
    variant="filled"
    fullWidth
    onChange={ onChange }
  />
);

MovieInputSearchComponent.propTypes = {
  onChange: PropTypes.func,
};

export default MovieInputSearchComponent;