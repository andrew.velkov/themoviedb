import React from 'react';
import PropTypes from 'prop-types';
import CircularProgress from '@material-ui/core/CircularProgress';
import styles from '../../../styles/fetching.module.scss';

const Fetching = ({ color = 'secondary', size = 45, thickness = 3, isFetching, children }) => (
  <article className={ styles.fetching }>
    {isFetching && <div className={ styles.fetching__wrap }>
      <CircularProgress color={ color } size={ size } thickness={ thickness } />
    </div>}

    { children }
  </article>
);

Fetching.propTypes = {
  color: PropTypes.string,
  size: PropTypes.number,
  thickness: PropTypes.number,
  isFetching: PropTypes.bool,
  children: PropTypes.any,
};

export default Fetching;
