import React from 'react';
import MovieSearchContainer from '../../containers/MovieSearchContainer';

const MovieSearchPage = () => (
  <MovieSearchContainer />
);

export default MovieSearchPage;
