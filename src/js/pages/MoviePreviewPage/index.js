import React from 'react';
import MoviePreviewContainer from '../../containers/MoviePreviewContainer';

const MoviePreviewPage = ({...props}) => (
  <MoviePreviewContainer {...props} />
);

export default MoviePreviewPage;
