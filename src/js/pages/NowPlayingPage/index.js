import React from 'react';
import NowPlayingContainer from '../../containers/NowPlayingContainer';

const NowPlayingPage = () => (
  <NowPlayingContainer />
);

export default NowPlayingPage;
