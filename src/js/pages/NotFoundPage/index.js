import React from 'react';
import { Link } from 'react-router-dom';

const logo = require('../../../assets/images/logo.svg');

const NotFoundPage = () => (
  <section style={{display: 'flex', height: '100%'}}>
    <article style={{margin: 'auto'}}>
      <img style={{maxWidth: 400}} src={ logo } alt="Page not found" />
      <h2>Page not found! <Link to="/"> Back to home</Link></h2>
    </article>
  </section>
);

export default NotFoundPage;
