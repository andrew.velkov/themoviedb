This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## themoviedb

![alt text](http://i.piccy.info/i9/1268531cd9526bc4e7734038a35410f4/1547453551/271626/1289884/Screenshot_6.png)

### `git clone https://gitlab.com/andrew.velkov/themoviedb.git`

### `cd themoviedb`

### `yarn install`

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


### `yarn build`